//
//  TableViewController.m
//  testPlayer
//
//  Created by Kriss Violense on 04.04.16.
//  Copyright © 2016 Kriss_Violense. All rights reserved.
//

#import "TableViewController.h"
#import "TableViewCell.h"
#import <AFNetworking/AFNetworking.h>
#import <AFNetworkReachabilityManager.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "TrackViewController.h"

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    TrackCountInView = 10;
    allResponseJSON = [[NSMutableArray alloc] init];
    self.title =@"CONNECTING...";
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self getRequestTrack:10 fromHere:TrackCountInView - 10];
    });
    
    
    
    

}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TrackViewController * TrackVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TrackVC"];

    _currentTrack = indexPath.row;
    _allSound = allResponseJSON;
    
    TrackVC.allSound = _allSound;
    TrackVC.currentTrack = _currentTrack;
    
    [self.navigationController pushViewController:TrackVC animated:YES];
}

-(void)getRequestTrack:(int) limitTrack fromHere:(int)from
{
    NSString *addres = [NSString stringWithFormat:@"https://api-content-beeline.intech-global.com/public/marketplaces/1/tags/10/melodies?limit=%d&from=%d", limitTrack, from];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReturnCacheDataElseLoad];
    [manager GET:addres parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        pageResponseJSON = [responseObject valueForKey:@"melodies"];
        [self mapping:pageResponseJSON];
        [self.tableView reloadData];
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    
}

-(void) mapping: (NSArray *)parseJSON {
    for(int i = 0; i < parseJSON.count; i ++) {
        Track *tmpTrac = [[Track alloc] init];
        [tmpTrac mapping:tmpTrac :parseJSON[i]];
        [allResponseJSON addObject:tmpTrac];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return allResponseJSON.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    self.title =@"INTECH PLAYER";
    
    BOOL flag = [AFNetworkReachabilityManager sharedManager].reachable;
    NSLog(flag ? @"Yes" : @"No");
    
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    Track *responseObject = allResponseJSON[indexPath.row];
    
    
    cell.titleLabel.text = responseObject.groupName;
    cell.TrackNameLabel.text = responseObject.trackName;
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:[NSURL URLWithString: responseObject.urlLogoTrack]
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                [cell.LogoImView setImage:image];
                            }
                        }];
    if (indexPath.row >= TrackCountInView - 1)
    {
        self.title =@"UPDATING...";
        [self showNext10Track];
    }
    return cell;
    
}

- (void) showNext10Track {
    [self getRequestTrack: 10 fromHere:TrackCountInView];
    TrackCountInView += 10;

}

@end
