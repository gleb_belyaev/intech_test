//
//  Track.h
//  testPlayer
//
//  Created by Kriss Violense on 06.04.16.
//  Copyright © 2016 Kriss_Violense. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Track : NSObject

@property (nonatomic, copy) NSString *trackName;
@property (nonatomic, copy) NSString *groupName;
@property (nonatomic, copy) NSString *urlLogoTrack;
@property (nonatomic, copy) NSString *urlSound;

-(void) mapping: (Track *) track :(NSString *) JSON;

@end
