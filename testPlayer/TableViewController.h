//
//  TableViewController.h
//  testPlayer
//
//  Created by Kriss Violense on 04.04.16.
//  Copyright © 2016 Kriss_Violense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Track.h"

@interface TableViewController : UITableViewController {
    NSArray * pageResponseJSON;
    NSMutableArray *allResponseJSON;
    int TrackCountInView;
}

@property (assign,nonatomic) NSMutableArray *allSound;
@property (assign,nonatomic) long currentTrack;

-(void) getRequestTrack: (int) limit fromHere:(int)from;
-(void) showNext10Track;

@end
