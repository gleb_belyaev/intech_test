//
//  TrackView.m
//  testPlayer
//
//  Created by Kriss Violense on 06.04.16.
//  Copyright © 2016 Kriss_Violense. All rights reserved.
//

#import "TrackViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <MediaPlayer/MPNowPlayingInfoCenter.h>
#import <MediaPlayer/MPMediaQuery.h>
#import <AFNetworking/AFNetworking.h>
#import <AFNetworkReachabilityManager.h>

@implementation TrackViewController
AVPlayer *player;
AVPlayerItem *playerItem;
Track *currentTrac;

-(void) viewDidLoad {
    [super viewDidLoad];

    currentTrac = _allSound[_currentTrack];
    [self updateUI];
    [_NextTrack setEnabled:NO];
    [_Play setEnabled:NO];
    [_PrewTrack setEnabled:NO];
    
    [_PrewTrack setImage:[UIImage imageNamed:@"Rewind-50.png"] forState:UIControlStateNormal];
    [_NextTrack setImage:[UIImage imageNamed:@"Fast Forward-50.png"] forState:UIControlStateNormal];
    [_Play setImage:[UIImage imageNamed:@"Play-50.png"] forState:UIControlStateNormal];
    
}

- (IBAction)prevTrac:(id)sender {
    if (_currentTrack > 0)
        _currentTrack --;
    else
        _currentTrack = _allSound.count - 1;
    
    [self updateUI];
}

- (IBAction)play:(id)sender {
    if ((player.rate != 0) && (player.error == nil)) {
        [player pause];
        [_Play setImage:[UIImage imageNamed:@"Play-50.png"] forState:UIControlStateNormal];
    }else {
        [player play];
        [_Play setImage:[UIImage imageNamed:@"Pause-50.png"] forState:UIControlStateNormal];
    }
}

- (IBAction)nextTrac:(id)sender {
    [_nowPlayingTimeScrubber setValue:0.0f];
    if (_currentTrack + 1 < _allSound.count)
        _currentTrack ++;
    else
        _currentTrack = 0;
    
    [self updateUI];
}

-(IBAction)playTrack:(id)sender {
    
    [_Play setEnabled:YES];
    [_PrewTrack setEnabled:YES];
    [_NextTrack setEnabled:YES];
    CMTime duration = playerItem.duration;
    CMTime interval = CMTimeMakeWithSeconds(1.0, NSEC_PER_SEC);
    [player addPeriodicTimeObserverForInterval:interval queue:NULL usingBlock:^(CMTime time) {
        if ( CMTimeGetSeconds(playerItem.currentTime) > 0.0f && duration.value != 0) {
            [_nowPlayingTimeScrubber setValue: CMTimeGetSeconds(playerItem.currentTime)];
            _DuringTime.text = [NSString stringWithFormat:@"-%d:%d", ((int) round(CMTimeGetSeconds(duration)) / 60),(int) round(CMTimeGetSeconds(duration)) - (int) round(CMTimeGetSeconds(playerItem.currentTime))];
            
        }
    }];
    [_Play setImage:[UIImage imageNamed:@"Pause-50.png"] forState:UIControlStateNormal];
    if (duration.value != 0) {
        
        [_nowPlayingTimeScrubber setMinimumValue:0.0f];
        [_nowPlayingTimeScrubber setMaximumValue:(float) round(CMTimeGetSeconds(duration))];
        
    }
    [player play];
    
}

- (IBAction)chacgeVolume:(id)sender {
    player.volume = self.volumeSond.value;
}

-(IBAction) timeScrubberChange:(id) sender{
    
    CMTime currentTime = CMTimeMake(_nowPlayingTimeScrubber.value, 1);
    NSLog(@"%f", (float) round(CMTimeGetSeconds(currentTime)));
    [player seekToTime:currentTime];
}


-(void) updateUI {
    
    
    
    _DuringTime.text = @"0:0";
    currentTrac = _allSound[_currentTrack];
    self.TrackNameLbl.text = currentTrac.trackName;
    self.groupNameLbl.text = currentTrac.groupName;
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:[NSURL URLWithString:currentTrac.urlLogoTrack]
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                logoImg = image;
                                [self.LogoIV setImage:logoImg];
                                
                            }
                            
                        }];
    
    [self.volumeSond setValue:[[AVAudioSession sharedInstance] outputVolume]];
    
    NSString *soundFilePath = currentTrac.urlSound;
    NSURL * url= [NSURL URLWithString:soundFilePath];
    playerItem = [AVPlayerItem playerItemWithURL:url];
    player = [AVPlayer playerWithPlayerItem:playerItem];
    
    [player setVolume:_volumeSond.value];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(nextTrac:) name:AVPlayerItemDidPlayToEndTimeNotification object:[player currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playTrack:) name:AVPlayerItemTimeJumpedNotification object:[player currentItem]];

    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    
}

@end
