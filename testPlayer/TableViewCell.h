//
//  TableViewCell.h
//  testPlayer
//
//  Created by Kriss Violense on 04.04.16.
//  Copyright © 2016 Kriss_Violense. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *TrackNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *LogoImView;

@end
