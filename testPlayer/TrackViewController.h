//
//  TrackView.h
//  testPlayer
//
//  Created by Kriss Violense on 06.04.16.
//  Copyright © 2016 Kriss_Violense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Track.h"

@interface TrackViewController : UIViewController
{
    UIImage *logoImg;
}

- (IBAction)prevTrac:(id)sender;
- (IBAction)play:(id)sender;
- (IBAction)nextTrac:(id)sender;
- (IBAction)chacgeVolume:(id)sender;
- (IBAction)timeScrubberChange:(id)sender;

-(void)updateUI;

@property (weak, nonatomic) IBOutlet UILabel *DuringTime;
@property (weak, nonatomic) IBOutlet UISlider *nowPlayingTimeScrubber;
@property (weak, nonatomic) IBOutlet UISlider *volumeSond;
@property (weak, nonatomic) IBOutlet UIImageView *LogoIV;
@property (weak, nonatomic) IBOutlet UILabel *groupNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *TrackNameLbl;
@property (weak, nonatomic) IBOutlet UIButton *PrewTrack;
@property (weak, nonatomic) IBOutlet UIButton *Play;
@property (weak, nonatomic) IBOutlet UIButton *NextTrack;

@property (assign,nonatomic) long currentTrack;
@property (assign,nonatomic) NSMutableArray *allSound;

@end
