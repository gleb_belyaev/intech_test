//
//  Track.m
//  testPlayer
//
//  Created by Kriss Violense on 06.04.16.
//  Copyright © 2016 Kriss_Violense. All rights reserved.
//

#import "Track.h"

@implementation Track

-(void) mapping: (Track *) track :(NSString *) JSON {
    track.trackName = [JSON valueForKey:@"title"];
    track.groupName = [JSON valueForKey:@"artist"];
    track.urlLogoTrack = [JSON valueForKey:@"picUrl"];
    track.urlSound = [JSON valueForKey:@"demoUrl"];
    
    
}

@end
